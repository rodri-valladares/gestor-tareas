from django import forms
from .models import Tarea

class TareaForm(forms.ModelForm): #Con esto podemos asociar un formulario al modelo

    class Meta: #la clase meta le dice a la clase TareaForm como comportarse
        model = Tarea #le estoy indicando que voy a asociar el formulario a un modelo Tarea
        fields = ['tarea']