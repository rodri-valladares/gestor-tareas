from django.shortcuts import render, redirect
from .models import Tarea, TareaDeHoy
from .forms import TareaForm
from django.urls import reverse

def index(request):
    tareas = Tarea.objects.all()
    form = TareaForm()
    context = {'tareas':tareas,'form' : form}
    
    return render(request, "web/index.html", context=context)

def nueva_tarea(request):
    if request.method == "POST":
        form = TareaForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('index')
    else:
        form = TareaForm()
    context = {'form' : form}
    return render(request, "web/index.html", context=context)

def tarea_del_dia(request, tarea_id):
    if tarea_id == 8000:
        tareas_del_dia = TareaDeHoy.objects.all()
        context = {'tareas_del_dia':tareas_del_dia}
        return render(request, "web/tablero_del_dia.html", context=context)
    else:
        tarea = Tarea.objects.get(id=tarea_id)
        tarea_de_hoy = TareaDeHoy(
            tarea=tarea
        )
        tarea_de_hoy.save()
    
    return redirect('index')

def eliminar(request, tarea_id):
    tarea = Tarea.objects.get(id=tarea_id)
    
    tarea.delete()
    return redirect('index')

def eliminar_del_dia(request, tarea_id):
    tarea = TareaDeHoy.objects.get(id=tarea_id)
    tarea.delete()
    tareas_del_dia = TareaDeHoy.objects.all()
    context = {'tareas_del_dia':tareas_del_dia}
    return render(request, "web/tablero_del_dia.html", context=context)
