from django.urls import path
from django.conf.urls.static import static
from django.conf import settings

from . import views

urlpatterns = [
    path('', views.index, name='index'), #responde a la funcion index de view.py
    path("nueva_tarea/", views.nueva_tarea, name="nueva_tarea"), #response a la funcion nueva_tarea de view.py
    path("tarea_del_dia/<int:tarea_id>/", views.tarea_del_dia, name="tarea_del_dia"),
    path("eliminar/<int:tarea_id>/", views.eliminar, name="eliminar"),
    path("eliminar_del_dia/<int:tarea_id>/", views.eliminar_del_dia, name="eliminar_del_dia"),
]
urlpatterns+=static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)
